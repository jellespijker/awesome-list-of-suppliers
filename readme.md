# Awesome List Suppliers [![Awesome](https://cdn.rawgit.com/sindresorhus/awesome/d7305f38d29fed78fa85652e3a63e154dd8e8829/media/badge.svg)](https://github.com/sindresorhus/awesome)

> A curated list of suppliers


## Contents

- [General](#General)
- [Materials](#Materials)
  - [Aluminium extrusion](#Aluminium Extrusion)
- [Pumps](#Pumps)
- [Valves](#Valves)
- [Piping](#Piping)
- [Isolation](#Isolation)
- [Screw conveyors](#Screw conveyors)
- [Bulk Solids](#Bulk solids)
- [Landbouw](#Landbouw)
- [Mixers](#Mixers)
- [Heaters](#Heaters)
- [Weighing devices](#Weighing devices)
- [Electronic devices](#Electronics devices)
- [Sensors](#Sensors)
- [Measurement equipment](#Measurement equipment)
- [Lab equipment](#Lab equipement)
- [Construction & Assembly](#Construction & Assembly)
  - [Metal](Metal)
  - [Electronics](Electronics)
- [Brushes](#Brushes)

## General

- [norelem](https://www.norelem.com)
- [Eriks](https://shop.eriks.nl/nl/)

## Materials

- [Salomon's metalen producten](https://salomons-metalen.nl/producten) Special steels and alloys

### Aluminium Extrusion

- [Nedal Aluminium](https://www.nedal.com/)
- [Mifa](https://mifa.eu/nl/)
- [Boal](http://www.boalgroup.com/nl-nl)
- [Hydro](https://www.hydro.com/nl-nl)
- [Aliplast](https://www.aliplast.com/?o=be)

## Pumps

- [Sydex](http://www.sydexpump.com)
- [Albin](https://www.albinpump.com/)
- [Bedu](http://www.bedu.nl/)
- [Netzsch](https://pompen.netzsch.com/nl/)

## Valves

- [Econosto](https://www.econosto.nl/homepage)
- [Wouter Witzel](https://www.wouterwitzel.nl/)
- [WAMGROUP](http://wamgroup.com/en-GB/corporate/MFamily/350/Valves)
- [Hilto](http://hilto.nl)
- [ACS Valves](http://www.acsvalves.com/) Rotary valves

## Piping

- [RVM kunstoffen](https://www.rvmkunststoffen.nl/)  Easy around the corner.
- [Wolfard & Wessels](www.wolfard.n)
- [IHC Piping](https://www.royalihc.com/en/products/other-industries/piping)  Contact person Mario van de Berg
- [DJPS](http://www.djps.nl/) Quality. ask for Rob Vermeulen
- [Demaco](https://www.demaco.nl/) Cryogenic double walled pipes. High end quality stuf. FEM and cryogenic calculations.
- [Hilto](http://hilto.nl)
- [van Leeuwen](https://www.vanleeuwen.com/nl/) One of the main suppliers of IHC Piping
- [Salomon's metalen producten](https://salomons-metalen.nl/producten) Special tubes fine tolerances
- [Sandvik](https://www.materials.sandvik/en/products/tube-pipe-fittings-and-flanges/tubular-products/)
- [Fine tubes](https://www.finetubes.co.uk/products/applications/hplc-tubes)

## Isolation

- [Wiko](http://www.wijzijnwiko.nl/):
  Main supplier exhaust system isolation for IHC shipbuilding (mention Rein Ooms)

## Screw conveyors

- [WAMGROUP](http://wamgroup.com/en-GB/corporate/MFamily/306/Screw-Conveyors-Feeders)

## Bulk solids

- [WAMGROUP](http://wamgroup.com/en-GB/corporate/MFamily/350/Valves)

## Landbouw

- [Amacoo technische onderdelen](https://www.amacoo.nl/) technical parts used in farming industry, hydraulics

## Mixers

- [Agitaser](http://www.agitaser.com/)
- [List item](http://example.com)

## Heaters

- [Leister heaters](https://www.leister.com/nl)
- [resistencias](https://www.resistencias.com/eng/index.php) Intergrated heater tubes
- [ASA heat](https://asaheat.com/products/)
- [ThermoCoax](https://www.thermocoax.com/heating-elements/)
- [Omega Engineering](https://www.omega.com/en-us/c/industrial-heaters)
- [Thermoexpert](http://thermoexpert.com/products/)

## Weighing devices

- [Preciamolen](http://www.preciamolen.nl/):
  Good communication, willingness to think with you from the start. Contact persion: Kurt Heidsieck (email: <kurt.heidsieck@preciamolen.nl>)

## Electronic devices

- [Conrad](https://www.conrad.nl/)
- [RS Online](https://nl.rs-online.com/web/)
  Huge assortiment
- [Kiwi electronics](https://www.kiwi-electronics.nl/kiwi-electronics):
  Mostly "makers" stuff, for arduino's and raspberry PI etc. The actual shop is on the way between Delft and Kinderdijk. <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d9815.961989072764!2d4.339724000000001!3d52.043486!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x80df9985c2fc4db!2sKiwi+Electronics!5e0!3m2!1snl!2sus!4v1523892488839" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>


## Sensors

- [AE Sensors](https://aesensors.nl/) Good assortment and relatively cheap.
- [GE measurements](https://www.gemeasurement.com/) High end stuff but costly.
- [HBM](www.hbm.com)
- [Krohne](https://krohne.com/)
- [Turck](http://www.turck.nl/nl/)
- [WAMGROUP](http://wamgroup.com/en-GB/corporate/MFamily/354/Level-Pressure-Monitoring)

## Measurement equipment

- [Dewesoft](https://www.dewesoft.com/)

## Lab equipement

- [Thorlabs inc](https://www.thorlabs.com/)
- [Labmakelaar](https://www.labmakelaar.com/) 2nd hand lab equipement, they also buy equipement

## Construction & Assembly

### Metal
 - [Biezepol](https://www.biezepol.nl/)  Slow in communication
- [J&V Techniek](http://www.jv-techniek.nl)  Wim Verhaar (Boele B.V.) works here now
- [Blozo](https://blozo.nl/)
- [Ville Metaalindustrie B.V.](http://www.ville.nl/) Kantpersen, Walsen, knippen, lasersnijden, lassen
- [De Jong metaal en techniek](http://www.dejongmetaalentechniek.nl/) Lasersnijden, Knippen, zetten, ponsen, walsen, draaien ,frezen, boren, tappen, lassen
- [Jatec](http://www.jatec.nl/nl/) Hoge kwaliteit, iets duurder
 

### electronics
- [Jolectra](http://www.jolectra-service.nl/) Good service, worked on multiple projects, such as Crawler, Zunder plant.
- [van der Leun](https://www.vanderleun.nl/en/)


## Brushes

- [Koti](https://www.koti-eu.com/nl/home-borstels)

## Contribute

Contributions welcome! Read the [contribution guidelines](contributing.md) first.


## License

[![CC0](http://mirrors.creativecommons.org/presskit/buttons/88x31/svg/cc-zero.svg)](http://creativecommons.org/publicdomain/zero/1.0)

To the extent possible under law, Jelle Spijker has waived all copyright and
related or neighboring rights to this work.
